﻿using IPALogger = IPA.Logging.Logger;

namespace DLiveChat
{
    internal static class Logger
    {
        public static IPALogger log { get; set; }
    }
}
